<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.1/css/swiper.min.css">
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.1/js/swiper.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.1/js/swiper.min.js"></script>
    <style>
    
    .swiper-container 
{
    width: 100%;
    height: auto;
}

.swiper-slide {
	/* Center slide text vertically */
      display: -webkit-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      display: flex;
      -webkit-box-pack: center;
      -ms-flex-pack: center;
      -webkit-justify-content: center;
      justify-content: center;
      -webkit-box-align: center;
      -ms-flex-align: center;
      -webkit-align-items: center;
      align-items: center;
	background-repeat: no-repeat;
	background-position: center;
	background-size: contain;
}

.autoplay
{
	position: absolute;
	color: #000;
	font: 100px/100% arial, sans-serif;
	right: 15px;
	text-decoration: none;
	text-shadow: 0 1px 0 #fff;
	top: 15px;
	width:auto;
	height:auto;
	z-index:999;
	opacity:0.4;
	transition: opacity .1s ease-in, transform .1s ease-out; 
	animation: fade_close 3s ease-out;
}

@keyframes fade_close
{
	0% { opacity: 0.8; } 
	50% { opacity: 0.8; }
	to { opacity: 0.4; }
}

.autoplay:active
{
  transform:scale(0.8);
}

.stop
{
  right: 15px;
}

.play
{
  right: 90px;
}
    
    
    </style>
</head>
<body>
    
    <div class="swiper-container"> 
        <!-- <a onclick=mySwiper.autoplay.stop(); class="autoplay stop">||</a>
        <a onclick=mySwiper.autoplay.start(); class="autoplay play">▷</a> -->
            <div class="swiper-wrapper" id="gallery_wrapper">
              <div class="swiper-slide"><img src= "https://cdn.pixabay.com/photo/2018/01/18/19/06/time-3091031__340.jpg"></div>
          <div class="swiper-slide"><img src= "https://cdn.pixabay.com/photo/2017/09/30/22/16/rail-2803725__340.jpg"></div>
          <div class="swiper-slide"><img src= "https://cdn.pixabay.com/photo/2017/06/06/06/03/freezing-earth-2376303__340.jpg"></div>
            </div> 
            <div class="swiper-button-next"></div>  
            <div class="swiper-button-prev"></div>  
            <div class="swiper-pagination"></div> 
        
        </div>






    <script>
    
    var mySwiper = new Swiper ('.swiper-container', 
	{
    speed:1000,
		direction: 'horizontal',
		navigation: 
		{
		  nextEl: '.swiper-button-next',
		  prevEl: '.swiper-button-prev',
		},
		pagination: 
		{
			el: '.swiper-pagination',
			dynamicBullets: true,
		},
		zoom: true,
		keyboard: 
		{
			enabled: true,
			onlyInViewport: false,
		},
		mousewheel: 
		{
			invert: true,
		},
    autoplay: 
    {
      delay: 2000,
    },
    loop: true,
	}); 
    
    </script>
</body>
</html>